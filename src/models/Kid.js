const { Schema, model} = require('mongoose');


const kidSchema = new Schema({
    userID: String,
    name: String,
    username: String,
    pin: Number,
    age: Number
})

module.exports = model('Kid',kidSchema);