const { Schema, model} = require('mongoose');
const bcrypt = require('bcryptjs');

const userSchema = new Schema({
    email: String,
    password: String,
    name: String,
    lastname: String,
    country: String,
    birth: Date,
    phone: Number,
    verified: {type: Boolean, default: false}
})

// Encrypt the password
userSchema.methods.encryptPassword = async (password) =>{
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt);
};
// Validate password

userSchema.methods.validatePassword = function(password){
    // compare passwords
    return bcrypt.compare(password,this.password);
}
module.exports = model('User',userSchema);