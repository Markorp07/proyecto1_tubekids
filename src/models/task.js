const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const VideoSchema = new Schema({
    userID: String,
    name: String,
    url: String
});

module.exports = mongoose.model('video',VideoSchema);