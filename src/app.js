const path = require('path');
const express = require('express');
const BodyParser = require("body-parser");
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const app = express();

// Conecting to db
mongoose.connect('mongodb://localhost/tubeKids',{
    useNewUrlParser:true,
    useUnifiedTopology: true
})
    .then(db  => console.log('Db connected'))
    .catch(err => console.log(err));

// importing routes
const indexRoutes = require('./routes/index');

//setings
// app.set('views', path.join(__dirname,'views'));
// app.set('view engine', 'ejs');
// middelwares
app.use(express.urlencoded({ extended: false}));
app.use(BodyParser.json());
app.use(express.static(__dirname+'/public'));
// routes
app.use('/', indexRoutes);
app.use(require('./routes/authController'));
app.use(require('./routes/kidController'));
// Starting server
app.listen(3001, () =>{
    console.log('Server on port 3001');
});