const { Router } = require('express');
const jwt = require('jsonwebtoken');
const router = Router();
const User = require('../models/User');
const Kid = require('../models/Kid');
const verifyToken = require('./verifyToken');

// Register 
router.post('/register', async(req,res,next) =>{
    const { email,password,name,lastname,country,birth,phone,verified }= req.body;
    const user = new User({
        email,
        password,
        name,
        lastname,
        country,
        birth,
        phone,
        verified
    });
    user.password = await user.encryptPassword(user.password)
    await user.save();
    // Create token
    const token = jwt.sign({id: user._id},'my_secret_key',{
        expiresIn:60*60*24
    })

    res.json({auth: true, token});
});

// Get information
router.get('/me',verifyToken, async(req,res,next) =>{
    const user = await User.findById(req.userId, {password: 0});
    if(!user){
        return res.status(404).send('No user found');
    }
    res.json(user);
});

// Login 
router.post('/login', async(req,res,next) =>{
    const {email,password} = req.body;
    const user = await User.findOne({email:email})
    if(!user){
        res.status(404).send("The email doesn't exist");
    }
    const validPassword = await user.validatePassword(password);
    if(!validPassword){
        return res.status(401).json({auth:false, token: null});
    }
    const token = jwt.sign({id: user._id},'my_secret_key',{
        expiresIn: 60*60*24
    });
    res.json({auth:true, token, user});
});
// Login 
router.post('/logink', async(req,res) =>{
    let username = req.body.username;
    let pin = req.body.pin;
    Kid.findOne({username:username, pin:pin},function(err,kid){
        if(err){
            console.log(err);
            return res.status(500).send();
        }
        if(!kid){
            return res.status(404).send("The kid doesn't exist");
        }
        res.json({kid});
    })
});


module.exports = router;