const express = require('express');
const router = express.Router();
const Task = require('../models/task');
const User = require('../models/User');
// Create new resource
router.post('/video', async(req,res) =>{
    try{
        const task = new Task(req.body);
        let result = await task.save();
        res.send(result);
        // res.redirect('/');
    }
    catch(error){
        res.status(500).send(error);
    }
});
// Get all videos
router.get('/video/:id', async(req,res) =>{
    try {
        var id = req.params.id;
        const tasks = await Task.find({userID:id}).exec();
        res.send(tasks);
        
    } catch (error) {
        res.status(500).send(error);
    }
});
// Get videos by id
router.get('/video/:id', async(req,res) =>{
    try {
        let video = await Task.findById(req.params.id).exec();
        res.send(video);        
    } catch (error) {
        res.status(500).send(error);
    }
});
// Update videos
router.put('/video/:id', async(req,res) =>{
    try {
        let video = await Task.findById(req.params.id).exec();
        video.set(req.body);
        let result = await video.save();
        res.send(result);
    } catch (error) {
        res.status(500).send(error);
    }
});
// Delete videos
router.delete('/video/:id', async(req,res) =>{
    try {
        let result = await Task.deleteOne({_id: req.params.id}).exec();
        res.send(result);        
    } catch (error) {
        res.status(500).send(error);
    }
});
module.exports = router;