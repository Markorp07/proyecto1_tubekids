const express = require('express');
const router = express.Router();
const Kid = require('../models/Kid');

// Create new resource
router.post('/kid', async(req,res) =>{
    try{
        const kid = new Kid(req.body);
        let result = await kid.save();
        res.send(result);
    }
    catch(error){
        res.status(500).send(error);
    }
});
// verify if username exist
router.get('/verifyk',async(req,res) =>{
    await Kid.findOne({username:req.body.username},(err,user)=>{
        if (err) res.status(500).send(error);
        if(user !== null){
            res.status(200).send(true);
        }else{
            res.status(500).send(false);
        }
    });
});
// Get all kids
router.get('/kid/:id', async(req,res) =>{
    try {
        var id = req.params.id;
        const kids = await Kid.find({userID:id}).exec();
        res.send(kids);
        
    } catch (error) {
        res.status(500).send(error);
    }
});
// Get kids by id
router.get('/kid/:id', async(req,res) =>{
    try {
        let kid = await Kid.findById(req.params.id).exec();
        res.send(kid);        
    } catch (error) {
        res.status(500).send(error);
    }
});
// Update kids
router.put('/kid/:id', async(req,res) =>{
    try {
        let kid = await Kid.findById(req.params.id).exec();
        kid.set(req.body);
        let result = await kid.save();
        res.send(result);
    } catch (error) {
        res.status(500).send(error);
    }
});
// Delete kids
router.delete('/kid/:id', async(req,res) =>{
    try {
        let result = await Kid.deleteOne({_id: req.params.id}).exec();
        res.send(result);        
    } catch (error) {
        res.status(500).send(error);
    }
});
module.exports = router;